import Foundation

//Реализовать структуру IOSCollection и создать в ней copy on write по типу

func address(off value: AnyObject) -> String {
  return "\(Unmanaged.passUnretained(value).toOpaque())"
}

struct IOSCollection {
    var id = 1
}

class Ref<T> { 
    var value : T
    init (value: T) {
        self.value = value
    } 
}

struct Container <T> { 
    var ref : Ref<T> 
    init(value: T) {
        self.ref = Ref(value: value) 
    }

    var value : T {
        get { 
            ref.value
        }

        set { 
            guard(isKnownUniquelyReferenced(&ref)) else { 

                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}
var id = IOSCollection()
var container1 = Container(value: id)
var container2 = container1

print(address(off: container1.ref)) //0x00005581af6c1230
print(address(off: container2.ref)) //0x00005581af6c1230

container2.value.id = 2

print(address(off: container1.ref)) //0x00005581af6c1230
print(address(off: container2.ref)) //0x00005581af6c1890

//Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол

protocol Hotel {
  init (roomCount : Int)
}

class HotelAlfa : Hotel {
  var roomCount : Int
  required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

// /Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'

protocol GameDice {
  var numberDice : Int { get }
}

extension Int : GameDice {
    var numberDice : Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCoub = 4
let res = diceCoub.numberDice

//Создать протокол с одним методом и 2 свойствами, одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство

@objc protocol Animal {
  var family : String { get }
  @objc optional var color : String { get }

  func checkAnimalFamily()
}

class Cat : Animal {
  var family : String
  
  init(family: String) {
    self.family = family
  }

  func checkAnimalFamily() {
    print("This cat is in \(self.family) family")
  }
}

let cat = Cat(family: "cat")
let res2 = cat.checkAnimalFamily()

//Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
//Компании подключаем два этих протокола
//Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.

enum Platform {
  case ios
  case android
  case web
}

protocol CodeWriting {
  var time : Int { get }
  var codeCount : Int { get }
  func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol CodeStopping {
  func stopCoding()
}

class Company : CodeWriting, CodeStopping {
  var numberOfSpecialist : Int
  var platform : Platform
  var codeCount : Int
  var time : Int

  init(numberOfSpecialist : Int, platform : Platform, codeCount : Int, time : Int) {
    self.numberOfSpecialist = numberOfSpecialist
    self.platform = platform
    self.codeCount = codeCount
    self.time = time
  }

  func writeCode(platform: Platform, numberOfSpecialist: Int) {
    if platform != self.platform {
      print("Компания не программирует для данной платформы")
    } 
    if numberOfSpecialist < 5 {
      print("Команда не успеет в срок")
    } else {
      print("Разработка началась. пишем код для \(platform).")
    }
  }

  func stopCoding() {
    print("Работа закончена. Сдаю в тестирование")
  }
  
}

let company1 = Company(numberOfSpecialist: 10, platform : Platform.ios, codeCount : 10, time : 12)

let res3 = company1.writeCode(platform: Platform.android, numberOfSpecialist: 2)



