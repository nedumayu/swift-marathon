//Создайте по 2 enum с разным типом RawValue

enum Weather : String {
  case winter = "cold"
  case spring = "warm"
  case summer = "hot"
  case autumn = "rainy"
}

enum DisplayWidth : Int {
  case phone = 320
  case tablet = 768
  case laptop = 1280
  case monitor = 1920
}

//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж

enum Sex {
  case men
  case women
}

enum Age {
  case young
  case adult
  case middleAged
  case elderly
}

enum WorkExperience {
  case junior
  case middle
  case senior
}

//Создать enum со всеми цветами радуги

enum RainbowColors {
  case red
  case orange
  case yellow
  case green
  case blue
  case darkBlue
  case purple
}

//Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.

let stuff = ["strawberry", "peach", "sun", "grass", "sky", "sea", "pepper"]
func printEnum (rainbow : RainbowColors) {
  switch rainbow {
    case .red: 
      print("red \(stuff[0])")
    case .orange: 
      print("orange \(stuff[1])")
    case .yellow: 
      print("yellow \(stuff[2])")
    case .green: 
      print("green \(stuff[3])")
    case .blue: 
      print("blue \(stuff[4])")
    case .darkBlue: 
      print("darkBlue \(stuff[5])")
    case .purple: 
      print("purple \(stuff[6])")
  }
}

printEnum(rainbow: RainbowColors.green)

// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки

enum Score {
  case five
  case four
  case three
  case two
}

func rate(score: Score) {
  switch score {
    case .five:
      print("Excellent mark")
    case .four:
      print("Good mark")
    case .three:
      print("Satisfactory score")
    case .two:
      print("Unsatisfactory grade")
  }
}

rate(score: Score.three)

//Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum

enum Cars {
	case compact
  case golfCar
  case minivan
	case sportsCar
  case superCar
}

func checkGarage(cars: Cars) {
  switch cars {
    case .compact: 
      print("Compact car is in garage")
    case .golfCar: 
      print("GolfCar car is in garage")
    case .minivan: 
      print("Minivan is in garage")
    case .sportsCar: 
      print("Sports car is in garage")
    case .superCar: 
      print("Supercar is in garage")
  }
}

checkGarage(cars: Cars.superCar)