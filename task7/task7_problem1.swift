//Ошибка данного кода заключается в том, что даже при удалении ссылок на объект сам объект не удаляется из памяти и сообщение об удалении не выводится. Исправить это можно, сделав одну из переменных "слабой"

class Car {
  weak var driver: Man?
    
  deinit {
      print("Машина удалена из памяти")
  }
}

class Man {
  var myCar: Car?
    
  deinit {
      print("Мужчина удален из памяти")
  }
}

var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car

car = nil
man = nil

