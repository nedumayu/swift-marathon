//Ошибка заключается в том, что даже при удалении ссылок на объект сам объект не удаляется из памяти и сообщение об удалении не выводится. 
//Исправить это можно, добавив клоужер

class Man {
  var passport: Passport?
  deinit {
    print("man deleted")
  }
}

class Passport {
  let man: Man
  init(man: Man) {
    self.man = man
  }
  deinit {
    print("passport deleted")
  }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)

var closure : (() -> ())?
closure = {
  man?.passport = passport
}

passport = nil 
man = nil 
