//Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать марку авто, год выпуска, объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника

// Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия

struct Car : Hashable{
  var model : String
  var manufactureYear : Int
  var trunkVolume : Int
  var isEngineRunning : Bool
  var isWindowOpen : Bool
  var filledTrunkVolume :  Int

  mutating func startTrip(action: Action) {
    if action == Action.runEngine {
      isEngineRunning = true
    }
    if action == Action.stopEngine {
      isEngineRunning = false
    }
    if action == Action.openWindow {
      isWindowOpen = true
    }  
    if action == Action.closeWindow {
      isWindowOpen = false
    }
    
  }
}

struct Truck : Hashable {
  var model : String
  var manufactureYear : Int
  var trunkVolume : Int
  var isEngineRunning : Bool
  var isWindowOpen : Bool
  var filledTrunkVolume :  Int

  mutating func loadTruck (action: Action, volume: Int) {
    if action == Action.loadCargo {
      if volume < (trunkVolume - filledTrunkVolume) {
          filledTrunkVolume += volume
        }
      else {
        print("only \(trunkVolume - filledTrunkVolume) kilograms of free space left")
      }
    }   
    if action == Action.unloadCargo {
      filledTrunkVolume -= volume
    }
  }
}

//Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, открыть/закрыть окна, погрузить/выгрузить из кузова/багажника груз определенного объема

enum Action {
  case runEngine
  case stopEngine
  case openWindow
  case closeWindow
  case loadCargo
  case unloadCargo
}

//Инициализировать несколько экземпляров структур. Применить к ним различные действия. Положить объекты структур в словарь как ключи, а их названия как строки например var dict = [structCar: 'structCar']

var car = Car(model: "BMW", manufactureYear: 2021, trunkVolume: 10, isEngineRunning: false, isWindowOpen: false, filledTrunkVolume: 3)
var truck = Truck(model: "Ford", manufactureYear: 2019, trunkVolume: 40, isEngineRunning: true, isWindowOpen: true, filledTrunkVolume: 0)

car.startTrip(action: Action.runEngine)
car.startTrip(action: Action.openWindow)
truck.loadTruck(action: Action.loadCargo, volume: 4)

var dict = [AnyHashable: Any]()
dict[AnyHashable(car)] = "car"
dict[AnyHashable(truck)] = "truck"
print(dict)

