let name : String = "Alice"
let surname : String = "Sazhina"
let age : Int = 22

let university : String = "Saint Petersburg Electrotechnical University 'LETI'"
let course : Int = 4

let hobby : [String] = ["music", "art"]
let likeCats : Bool = true

print("About me:")
print("My name is \(name) \(surname). I'm \(22) years old.")
print("I am a \(course)th year student at \(university).")
print("My hobbies are \(hobby[0]) and \(hobby[1]). And I \(likeCats ? "like" : "dont like") cats.")