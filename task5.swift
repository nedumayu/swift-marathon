import Foundation

//Создать класс родитель и 2 класса наследника

class Animal {
  let family : String
  let genus : String
  let species : String

  init(family: String, genus: String, species: String) {
    self.family = family
    self.genus = genus
    self.species = species
  }
}

class Bird : Animal {
  var featherColor : String?
}

class Herbivores : Animal {
  var legCount : Int?
  var isAgressive : Bool?
}

//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)

class House {
  let width : Int
  let height : Int

  init(width: Int, height: Int) {
    self.width = width
    self.height = height
  }
  
  func create() {
    print("Room area is \(self.width*self.height)")
  }

  func destroy() {
    print("The house was destroyed")
  }
}

let house = House(width: 20, height: 30)
house.create()

//Создайте класс с методами, которые сортируют массив учеников по разным параметрам

class School {
  
  func sortByMarksAsc(array: [Int]) -> [Int] {
    let sortedArray = array.sorted(by: { $0 < $1})
    return sortedArray
  }

  func sortByNamesLength(array: [String]) -> [String] {
    let sortedArray = array.sorted(by: { $0.count < $1.count})
    return sortedArray
  }
  
}

let studentsMark = [3, 5, 4, 5, 1, 2]
let students = School()
print(students.sortByMarksAsc(array: studentsMark))
let studentNames = ["Alex", "Mike", "Herald", "Bob"]
let students2 = School()
print(students2.sortByNamesLength(array: studentNames))

//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов

//Класс
class HumanClass {
  var name : String

  init(name: String) {
    self.name = name
  }
}

var humanClass1 = HumanClass(name: "Alice")
var humanClass2 = humanClass1
humanClass2.name = "Alex"
print(humanClass1.name)
print(humanClass2.name)

//Структура
struct HumanStruct {
  var name : String
}

var humanStruct1 = HumanStruct(name: "Alice")
var humanStruct2 = humanStruct1
humanStruct2.name = "Alex"
print(humanStruct1.name)
print(humanStruct2.name)

//Различия стукруры и класса:
//Классы являются ссылочным типом данных, а структуры - значимыми, то есть при копировании классов оба экземпляра ссылаются на один и тот же объект, а при копировании структур получаются две уникальные копии.
//Структуры можно объявлять без инициализатора, а классы нет.
//Классы могут наследоваться, а структуры нет.

//Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
//Сохраняйте комбинации в массив
//Если выпала определённая комбинация - выводим соответствующую запись в консоль
//Результат в консоли примерно такой: 'У вас бубновый стрит флеш'.

enum Suit: String {
    case hearts = "черви"
    case diamonds = "бубны"
    case clubs = "трефы"
    case spades = "пики"
    
    static let allSuits = [hearts, diamonds, clubs, spades]
}

enum Rank: Int {
    case two = 2, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king, ace
    
    static let allRanks = [two, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace]
}

struct Card {
    let rank: Rank
    let suit: Suit
}

func generateDeck() -> [Card] {
    return Suit.allSuits.flatMap { suit in
        Rank.allRanks.map { rank in
            Card(rank: rank, suit: suit)
        }
    }
}

func dealCards(numCards: Int) -> [Card] {
    let deck = generateDeck().shuffled()
    return Array(deck.prefix(numCards))
}

class PokerHand {
    var cards: [Card]
    
    init(cards: [Card]) {
        self.cards = cards
    }
    
    var isFlush: Bool {
        let suit = cards[0].suit
        return cards.allSatisfy { $0.suit == suit }
    }
    
    var isStraight: Bool {
        let sortedRanks = cards.map { $0.rank.rawValue }.sorted()
        return sortedRanks == [2, 3, 4, 5, 14] || (sortedRanks[0] - sortedRanks[4] == 4)
    }
    
    var isStraightFlush: Bool {
        return isFlush && isStraight
    }
    
    var isRoyalFlush: Bool {
        return isFlush && cards.map { $0.rank.rawValue } == [10, 11, 12, 13, 14]
    }
    
    var isFourOfAKind: Bool {
        let rankCounts = Dictionary(grouping: cards, by: { $0.rank }).mapValues { $0.count }
        return rankCounts.values.contains(4)
    }
    
    var isFullHouse: Bool {
        let rankCounts = Dictionary(grouping: cards, by: { $0.rank }).mapValues { $0.count }
        return rankCounts.values.sorted() == [2, 3]
    }
    
    var isThreeOfAKind: Bool {
        let rankCounts = Dictionary(grouping: cards, by: { $0.rank }).mapValues { $0.count }
        return rankCounts.values.contains(3)
    }
    
    var isTwoPair: Bool {
        let rankCounts = Dictionary(grouping: cards, by: { $0.rank }).mapValues { $0.count }
        return rankCounts.values.sorted() == [1, 2, 2]
    }
    
    var isOnePair: Bool {
        let rankCounts = Dictionary(grouping: cards, by: { $0.rank }).mapValues { $0.count }
        return rankCounts.values.contains(2)
    }
    
    func evaluateHand() -> String {
        if isRoyalFlush {
            return "У вас роял-флеш"
        } else if isStraightFlush {
            return "У вас стрит-флеш"
        } else if isFourOfAKind {
            return "У вас каре"
        } else if isFullHouse {
            return "У вас фул-хаус"
        } else if isFlush {
            return "У вас флеш"
        } else if isStraight {
            return "У вас стрит"
        } else if isThreeOfAKind {
            return "У вас сет"
        } else if isTwoPair {
            return "У вас две пары"
        } else if isOnePair {
            return "У вас одна пара"
        } else {
            let sortedRanks = cards.map { $0.rank.rawValue }.sorted().reversed()
            let rankStrings = sortedRanks.map { String($0) }
            return "У вас высшая карта: " + rankStrings.joined(separator: " ")
        }
    }
}

let hand = PokerHand(cards: dealCards(numCards: 5))
let handString = hand.cards.map { "\($0.rank) \($0.suit.rawValue)" }.joined(separator: ", ")
let evaluation = hand.evaluateHand()
print("Ваши карты: \(handString)")
print(evaluation)
       