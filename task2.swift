//Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце

let daysInMonths : Array<Int> = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

//Создать массив элементов 'название месяцов' содержащий названия месяцев

let months : Array<String> = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"]

//Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)

for i in 0..<daysInMonths.count {
    print(daysInMonths[i])
}

//Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней

for i in 0..<daysInMonths.count {
    print(months[i], ":", daysInMonths[i])
}

//Сделайте то же самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)

let tuples = (months: months, days: daysInMonths)

for i in 0..<tuples.months.count {
  print(tuples.months[i], ":", tuples.days[i])
}

//Сделайте то же самое, только выводите дни в обратном порядке (порядок в массиве не менять)
var count : Int = months.count-1

for i in 0..<tuples.months.count {
    print(tuples.months[(i + count)], ":", tuples.days[i + count])
    count -= 2
}

//Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года

let day : Int = 29
let month : Int = 3
var daysFromStart : Int = 0

for i in 0..<(month - 1) {
    daysFromStart += daysInMonths[i]
}
daysFromStart += day
 
print(daysFromStart)

