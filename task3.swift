//Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную

var nums = [2, 3, 1, 55, 3, 4, 2]

func sorting(array : [Int], callback : (Int, Int) -> Bool) -> [Int] {
  let _sortedArray = nums.sorted(by: callback)
  return _sortedArray
}

var sortByDesc = sorting(array: nums) {(a: Int, b: Int) -> Bool in
   return a > b
}

var sortByAsc = sorting(array: nums) {(a: Int, b: Int) -> Bool in
   return a < b
}

//Вывести результат в консоль

print(sortByDesc)
print(sortByAsc)

//Создать метод, который принимает имена друзей, после этого имена положить в массив

var names : [String] = []
func makeNamesArray (name: String) -> [String] {  
  names.append(name)
  return names
}  
makeNamesArray(name: "Klava")
makeNamesArray(name: "Eva")
makeNamesArray(name: "Naruto")
makeNamesArray(name: "Ei")

print(names)

//Массив отсортировать по количеству букв в имени

var sortedNames = names.sorted(by: { (s1: String, s2: String) -> Bool in         return s1.count < s2.count })
print(sortedNames)

//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга

var namesDic = [Int : String] ()

for name in names {
  namesDic[name.count] = name
}

print(namesDic)

//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение

func printNamesDic (key : Int) {
  print(key, ":", namesDic[key])
}

printNamesDic(key: 3)

//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль

func isArrayEmpty (stringArray: [String], intArray: [Int]) {
  var stringArray = stringArray
  var intArray = intArray
  if stringArray.isEmpty {
    stringArray.append("hello world")
    print(stringArray)
  } 
  if intArray.isEmpty {
    intArray.append(3)
    print(stringArray)
  }
}

var strs : [String] = []
var ints : [Int] = [2, 1]
isArrayEmpty(stringArray: strs, intArray: ints)